# Worker and Scheduler for a Laravel app

This container runs Supervisord on Alpine to make sure your workers and your schedule are running always.

## Contents

This image is built upon the official PHP 7.3 FPM (Alpine flavour) Docker image.

In it are added:

- the Redis connector driver.
  
- Supercronic (for handling cron tasks with extra verbosity)
  

## How to

A docker-compose entry is as follows:

```yaml
services:

worker-scheduler:

image: andersonpem/laravel-worker-scheduler:1.0

environment:

# The number of worker processes to spawn

NUM_WORKERS: 2

# The application's document root

APP_DOCUMENT_ROOT: /var/www/html

volumes:

# Map the document root

- ./:/var/www/html

# Map a crontab file that will run with Supercronic

- ./cron:/etc/crontabs/root
```

## Environment variables

**NUM_WORKERS** specifies the number of worker proccesses that will be created by Supervisord.

**APP_DOCUMENT_ROOT** is the laravel root directory (not /public)

## Volumes

You must map your project's root directory to the same place where the environment variable **APP_DOCUMENT_ROOT** is on the container, like specified in the docker-compose example above.

**/etc/crontabs/root** is the crontab file that supercronic looks up when the container starts. Map a file with your cron routine for the project, like so:

```crontab
* * * * * cd /var/www/html && php artisan schedule:run >> /dev/null 2>&1
```

You must use crontab valid syntax on the file.

## Logging

Logs of this container are available in the following folders:

**/var/log/worker**: Supervisor log storage for the Worker processes.

**/var/log/scheduler**: Supervisor log storage for the SuperCronic and sheculer activities

**/var/log/supervisord**: Supervisord main logs and pid file.

## Audit

Source code for this image is available in this repository in the folder named .docker.

### Building from source

You can build with the following command:

```bash
docker build -f ./.docker/laravel-worker-scheduler/Dockerfile -t laravel-worker-scheduler  .
```

## License

This image is licensed under MIT License.

## Author

Anderson is a brazilian software developer and future DevOps engineer.